/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';

import { Provider as ReduxProvider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import store, { persistor } from "./src/store/Store";
import { ThemeProvider } from 'react-native-elements';
import Theme from "./src/const/Theme";
import Login from "./src/views/Auth/Login"

const App = () => {
  return (
    <ThemeProvider theme={Theme}>
      <ReduxProvider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Login />
        </PersistGate>
      </ReduxProvider>
      </ThemeProvider>
  );
};


export default App;
