import { createSlice } from "@reduxjs/toolkit";

const initialAuth = {
    token : null,
}

const AuthSlice = createSlice({
    name:'auth',
    initialState: initialAuth,
    reducers:{
        setToken: (state, action) => {
            state.token = action.payload;
        }
    }
})

export const actions = AuthSlice.actions;
export default AuthSlice.reducer;