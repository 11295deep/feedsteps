import { combineReducers } from "@reduxjs/toolkit";

import AuthReducer from "./Auth/Reducer";

const appReducer = combineReducers({
    auth:AuthReducer,
})

const rootReducer = (state,action) => {
    if(action?.type === 'auth/logout'){
        state = undefined
    }
    return appReducer(state,action);
}

export default rootReducer;