import {  } from "react-native-elements";

const Theme = {

    colors:{
        primary:"#0093E9",
        platform:{
            ios:{

            },
            android:{
                
            }
        }
    },
    Button:{
        raised:true,
    },
    Input:{

    },
    Text:{
        fontSize: 14,
        h1Style:[{
            fontSize: 30,
        }],
        h2Style:[{
            fontSize: 20,
        }],
        h3Style:[{
            fontSize: 18,
        }],
        h4Style:[{
            fontSize: 16,
        }],
    },
    

}

export default Theme;